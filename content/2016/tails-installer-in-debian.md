Title: Tails installer is now in Debian
Slug: tails-installer-in-debian
Date: 2016-02-11 14:30
Author: u
Tags: tails, privacy, anonymity, announce
Status: published

[Tails][tails-link] (The amnesic incognito live system) is a live OS based
on Debian GNU/Linux which aims at preserving the user's privacy and anonymity
by using the Internet anonymously and circumventing censorship. Installed on
a USB device, it is configured to leave no trace on the computer you are
using unless asked explicitly.

[![Tails Logo](|filename|/images/tails-logo-flat.png)][tails-link]

As of today, the people the most needy for digital security are not computer
experts. Being able to get started easily with a new tool is critical to its
adoption, and even more in high-risk and stressful environments. That's why we
wanted to make it faster, simpler, and more secure to install Tails for new
users.

One of the components of Tails, the [Tails Installer][tails-instaler] is now
in Debian thanks to the [Debian Privacy Tools Maintainers Team][team-link].

Tails Installer is a [graphical tool][screenshot] to install or upgrade Tails
on a USB stick from an ISO image. It aims at making it easier and faster to [get
Tails up and running][tails-installdoc].

The previous process for getting started with Tails was very complex and was
problematic for less tech-savvy users. It required starting Tails three
times, and copying the full ISO image onto a USB stick twice before having a
fully functional Tails USB stick with persistence enabled.

This can now be done simply by installing Tails Installer in your existing
Debian system, using sid, stretch or jessie-backports, plugging a USB stick
and choosing if one wants to update the USB stick or to install Tails using
a previously downloaded ISO image.

Tails Installer also helps Tails users to create an encrypted persistent
storage for personal files and settings in the rest of the available space.

[tails-link]: https://tails.boum.org
[tails-instaler]: https://tracker.debian.org/pkg/tails-installer
[screenshot]: https://screenshots.debian.net/package/tails-installer
[team-link]: https://qa.debian.org/developer.php?email=pkg-privacy-maintainers%40lists.alioth.debian.org
[tails-installdoc]: https://tails.boum.org/install


