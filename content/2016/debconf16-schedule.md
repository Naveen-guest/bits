Title: DebConf16 schedule available
Slug: debconf16-schedule
Date: 2016-06-27 09:00
Author: Laura Arjona Reina
Tags: debconf, debconf16, debian
Status: published

[DebConf16](https://debconf16.debconf.org/) will be held this and next week in Cape Town, South Africa, and we're happy to announce that the [schedule](https://debconf16.debconf.org/schedule/) is already available. Of course, it is still possible for some minor changes to happen!

The [DebCamp Sprints](https://debconf16.debconf.org/debcamp-sprints/) already started on 23 June 2016.

DebConf will open on Saturday, 2 July 2016 with the [Open Festival](https://debconf16.debconf.org/open-festival/), where events of interest to a wider audience are offered, ranging from topics specific to Debian to a wider appreciation of the open and maker movements (and not just IT-related). Hackers, makers, hobbyists and other interested parties are invited to share their activities with DebConf attendees and the public at the University of Cape Town, whether in form of workshops, lightning talks, install parties, art exhibition or posters. Additionally, a Job Fair will take place on Saturday, and its job wall will be available throughout DebConf.

The full schedule of the Debian Conference thorough the week is [published](https://debconf16.debconf.org/schedule/). After the Open Festival, the conference will continue with more than 85 [talks and BoFs](https://debconf16.debconf.org/talks/) (informal gatherings and discussions within Debian teams), including not only software development and packaging but also areas like translation, documentation, artwork, testing, specialized derivatives, maintenance of the community infrastructure, and other.

There will also be also a plethora of social events, such as our traditional cheese and wine party, our group photo and our day trip.

DebConf talks will be broadcast live on the Internet when possible, and videos of the talks will be published on the web along with the presentation slides.

DebConf is committed to a safe and welcome environment for all participants. See the [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml) and the [Debian Code of Conduct](https://www.debian.org/code_of_conduct) for more details on this.

Debian thanks the commitment of numerous [sponsors](https://debconf16.debconf.org/sponsors/) to support DebConf16, particularly our Platinum Sponsor [Hewlett Packard Enterprise](http://www.hpe.com/engage/opensource).

## About Hewlett Packard Enterprise 

[Hewlett Packard Enterprise](http://www.hpe.com/engage/opensource) actively participates in open source.
Thousands of developers across the company are focused on open source projects, and HPE sponsors and supports the open source community in a number of ways, including: contributing code, sponsoring foundations and projects, providing active leadership, and participating in various committees.
