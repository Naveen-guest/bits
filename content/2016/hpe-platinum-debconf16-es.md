Title: Hewlett Packard Enterprise, Patrocinador Platino de la DebConf16
Slug: hpe-platinum-debconf16
Date: 2016-03-08 12:00
Author: Laura Arjona Reina
Lang: es
Translator: Adrià García-Alzórriz
Tags: debconf16, debconf, sponsors, HPE
Status: published

[![HPElogo](|filename|/images/hpe.png)](http://www.hpe.com/engage/opensource)

Es un placer para nosotros anunciar que [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource) 
ha confirmado su apoyo a la [DebConf16](http://debconf16.debconf.org) como **Patrocinador Platino**.

*«Estamos emocionados al apoyar la conferencia anual de Debian, la cual
agrupa colaboradores de Debian de todo el mundo. Además de nuestro patrocinio,
participaremos activamente en la DebConf»*, dijo Steve Geary, Director 
Senior en Hewlett Packard Enterprise. 

HPE es una de las empresas de informática más grandes del mundo, que ofrece
un amplio catálogo de productos y servicios como servidores, soluciones de
almacenamiento, conectividad, consultoría y soporte, software y servicios
financieros.

HPE es también socio de desarrollo de Debian, y ofrece hardware para
el desarrollo de adaptaciones, réplicas de Debian y otros servicios de Debian 
(las donaciones de hardware se encuentran  listadas en la página de las 
[Máquinas Debian](https://db.debian.org/machines.cgi)).

Con esta confirmación adicional como Patrocinador Platino, HPE ayudará a 
hacer posible nuestra conferencia anual, y directamente apoya el progreso de
Debian y el software libre ayudando a fortalecer la comunidad que sigue
colaborando en los proyectos de Debian durante el resto del año.

¡Agradecemos a Hewlett Packard Enterprise su apoyo a la DebConf16!

## ¡Conviértase también en un patrocinador!

DebConf16 todavía busca patrocinadores.
Las empresas y organizaciones interesadas pueden contactar con el equipo
de la DebConf a través de [sponsors@debconf.org](mailto:sponsors@debconf.org), y
visitando la web de la DebConf16 en [http://debconf16.debconf.org](http://debconf16.debconf.org).
