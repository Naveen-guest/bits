Title: Novos desenvolvedores e mantenedores Debian (setembro e outubro de 2016)
Slug: new-developers-2016-10
Date: 2016-11-03 12:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian 
nos últimos dois meses:

  * Adriano Rafael Gomes (adrianorg)
  * Arturo Borrero González (arturo)
  * Sandro Knauß (hefee)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian 
nos últimos dois meses:

  * Abhijith PA
  * Mo Zhou
  * Víctor Cuadrado Juan
  * Zygmunt Bazyli Krynicki
  * Robert Haist
  * Sunil Mohan Adapa
  * Elena Grandi
  * Eric Heintzmann
  * Dylan Aïssi
  * Daniel Shahaf
  * Samuel Henrique
  * Kai-Chung Yan
  * Tino Mettler

Parabéns a todos!
