Title: Hewlett Packard Enterprise Platinum Sponsor of DebConf16
Slug: hpe-platinum-debconf16
Date: 2016-03-08 12:00
Author: Laura Arjona Reina
Tags: debconf16, debconf, sponsors, HPE
Status: published

[![HPElogo](|filename|/images/hpe.png)](http://www.hpe.com/engage/opensource)

We are very pleased to announce that [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource) 
has committed support to [DebConf16](http://debconf16.debconf.org) as a **Platinum sponsor**.

*"We're excited to support Debian's annual conference which brings
together Debian contributors from all around the world.  In addition
to our sponsorship, we will actively participate in DebConf"*, said
Steve Geary, Senior Director at Hewlett Packard Enterprise. 

HPE is one of the largest computer companies in the
world, providing a wide range of products and services, such as servers, storage, 
networking, consulting and support, software, and financial services.

HPE is also a development partner of Debian, 
and provides hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

With this additional commitment as Platinum Sponsor, 
HPE contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software 
helping to strengthen the community that continues to collaborate on 
Debian projects throughout the rest of the year.

Thank you very much Hewlett Packard Enterprise, for your support of DebConf16!

## Become a sponsor too!

DebConf16 is still accepting sponsors. 
Interested companies and organizations may contact the DebConf team 
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf16 website at [http://debconf16.debconf.org](http://debconf16.debconf.org).
