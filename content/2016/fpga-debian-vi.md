Title: Lập trình FPGA Tự Do với Debian
Date: 2016-12-22 18:15
Tags: fpga, programming
Slug: fpga-programming-debian
Author: Steffen Möller
Translator: Trần Ngọc Quân
Lang: vi
Status: published

[FPGA](https://wiki.debian.org/FPGA/) (Field Programmable Gate Array: mảng lô-gíc lập trình được)
ngày càng trở nên phổ biến trong việc thu thập dữ liệu, điều khiển thiết bị và
tăng tốc ứng dụng. Debian giờ đây có một bộ công cụ Tự Do và đầy đủ
để lập trình FPGA bằng ngôn ngữ Verilog, chuẩn bị tập tin nhị phân và thực
thi nó trên một thiết bị có giá cả phải chăng.

Xem [http://wiki.debian.org/FPGA/Lattice](http://wiki.debian.org/FPGA/Lattice)
để có thông tin chi tiết. Độc giả thân thuộc với
công nghệ có lẽ đoán ngay được rằng nó có liên quan đến
gói *yosys*
cùng với *berkeley-abc*, *arachne-"Place-and-Route"*
và công cụ *icestorm* kết nối với thiết bị.

Các gói được đóng góp bởi nhóm
[Debian Science](https://blends.debian.org/science/tasks/).

Chúng tôi hy vọng rằng nỗ lực hỗ trợ cộng đồng FPGA này
giúp góp nhặt một sự gia tăng số lượng kỹ năng để đẩy mạnh
trải nghiệm Nguồn Mở một cách thông suốt và làm giảm trở lực khi mới bước vào
công nghệ đầy thách thức này.
