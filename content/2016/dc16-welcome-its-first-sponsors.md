Title: DebConf16 welcomes its first nine sponsors!
Slug: dc16-welcome-its-first-sponsors
Date: 2016-03-30 17:00
Author: Laura Arjona Reina
Tags: debconf16, debconf, sponsors
Status: published

![DebConf16 logo](|filename|/images/Dc16_703x473.png)

DebConf16 will take place in Cape Town, South Africa in July 2016. We strive to
provide an intense working environment and enable good progress for Debian and
for Free Software in general. We extend an invitation to everyone to join us
and to support this event. As a volunteer-run non-profit conference, we depend
on our sponsors.

Nine companies have already committed to sponsor DebConf16! Let's introduce
them:

Our first Platinum sponsor is  [**Hewlett Packard Enterprise (HPE)**](http://www.hpe.com/engage/opensource).
HPE is one of the largest computer companies in the
world, providing a wide range of products and services, such as servers, storage, 
networking, consulting and support, software, and financial services.

HPE is also a development partner of Debian, 
and provides hardware for port development, Debian mirrors, and other Debian services
(hardware donations are listed in the [Debian machines](https://db.debian.org/machines.cgi) page).

Our first Gold sponsor is [**Valve**](http://www.valvesoftware.com/), 
 a company developing games, social entertainment platform, and game engine technologies.

Our second Gold sponsor is [**Google**](http://google.com), the technology company 
specialized in Internet-related services as online advertising and search engine.

[**Rusbitech**](http://www.astralinux.com/) 
(developers of the Astra Linux Debian derivative), 
[**credativ**](http://www.credativ.de/) 
(a service-oriented company focusing on open-source software and also a 
[Debian development partner](https://www.debian.org/partners/)), 
[**Catalyst**](https://catalyst.net.nz/) 
(a company offering IT solutions using open source software), 
the [**Bern University of Applied Sciences**](https://www.bfh.ch/) 
(with over [7,000](https://www.bfh.ch/en/bfh/facts_figures.html) students enrolled, located in the Swiss capital),
and [**Texas Instruments**](http://www.ti.com/) 
(the global semiconductor company) are our four Silver sponsors.

And last but not least, the open source company [**Univention**](https://www.univention.com/) 
has agreed to support us as Bronze-level.

## Become a sponsor too!

Would you like to become a sponsor? Do you know of or work in a company or
organization that may consider sponsorship?

Please have a look at our 
[sponsorship brochure](http://media.debconf.org/dc16/fundraising/debconf16_sponsorship_brochure.pdf)
(or a summarized [flyer](http://media.debconf.org/dc16/fundraising/debconf16_sponsorship_flyer.pdf)),
in which we outline all the details and describe the sponsor benefits. 

For further details, feel free to contact us through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf16 website at [https://debconf16.debconf.org](https://debconf16.debconf.org).
