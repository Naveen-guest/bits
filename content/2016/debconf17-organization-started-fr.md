Title: L'organisation de DebConf17 a débuté
Date: 2016-09-05 14:00
Tags: debconf, debconf17
Slug: debconf17-organization-started
Author: Laura Arjona Reina
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

La conférence DebConf17 aura lieu au collège de Maisonneuve à Montréal au
Canada du 6 août au 12 août 2017. Elle sera précédée d'un DebCamp, du
31 juillet au 4 août, et d'une Journée Debian, le 5 août.

Nous invitons tout le monde à nous rejoindre pour l'organisation de cette
conférence. Votre aide peut être précieuse dans différents domaines et nous
nous réjouissons de découvrir vos idées.

L'équipe chargé du contenu de DebConf est ouverte à vos suggestions pour
inviter des orateurs. Si vous souhaitez proposer quelqu'un qui n'est pas un
participant habituel de DebConf suivez les instructions détaillées dans le
billet [appel à propositions d'orateur](http://blog.debconf.org/blog/debconf17/2016-08-22_call_for_speakers.dc)
du blogue de DebConf.

Nous avons aussi commencé à contacter des parrains potentiels dans le monde
entier. Si vous connaissez des organismes qui pourraient être intéressés,
n'hésitez pas à leur transmettre la 
[brochure sur le parrainage](https://media.debconf.org/dc17/fundraising/debconf17_sponsorship_brochure_en.pdf)
ou à contacter [l'équipe de collecte de fonds](mailto:sponsors@debconf.org)
pour toutes pistes.

L'équipe de DebConf tient une réunion IRC toutes les deux semaines.
Veuillez consulter le [site](https://debconf17.debconf.org) de DebConf17 et
la [page du wiki](https://wiki.debconf.org/wiki/DebConf17), et suivre le
canal IRC ainsi que la liste de diffusion.

Travaillons ensemble, comme chaque année, à faire la meilleure DebConf
jamais réalisée !
