Title: Debian welcomes its 2014 GSoC students!
Slug: welcome-gsoc-students-2014
Date: 2014-04-22 11:00
Author: Nicolas Dandrimont
Tags: announce, gsoc
Status: published


We're excited to announce that [19 students have been selected](https://www.google-melange.com/gsoc/org2/google/gsoc2014/debian) 
to work with Debian during the Google Summer of Code this year!

Here is the list of accepted students and projects:

+ Abhishek Bhattacharjee: [AppStream/DEP-11 for the Debian Archive](https://wiki.debian.org/SummerOfCode2014/StudentApplications/AbhishekBhattacharjee)
+ Alexander Ovchinnikov: [Debian built with clang](https://wiki.debian.org/SummerOfCode2014/StudentApplications/Alexander)
+ Andrew Schurman: [Recursively building Java dependencies from source](https://wiki.debian.org/SummerOfCode2014/StudentApplications/AndrewSchurman)
+ Arthur Marble: [Debian built with clang](https://wiki.debian.org/SummerOfCode2014/StudentApplications/ArthurMarble)
+ Brandon Fairchild: [Debian Continuous Integration - Web Interface](https://wiki.debian.org/SummerOfCode2014/StudentApplications/BrandonF)
+ Clément Schreiner: [Provide some metrics in Debile](https://wiki.debian.org/SummerOfCode2014/StudentApplications/ClementSchreiner)
+ Dionysios Fryganas: [Project import/export for Alioth (FusionForge)](https://wiki.debian.org/SummerOfCode2014/StudentApplications/DionysiosFryganas)
+ Floris-Andrei Stoica-Marcu: [Get Muon ready](https://wiki.debian.org/SummerOfCode2014/StudentApplications/FlorisAndreiStoicaMarcu)
+ Ian S. Donnelly: [Improve Configuration Upgrade Mechansim with Elektra](https://wiki.debian.org/SummerOfCode2014/StudentApplications/IanSDonnelly)
+ Joseph Bisch: [Debian metrics portal](https://wiki.debian.org/SummerOfCode2014/StudentApplications/JosephBisch)
+ Juliana Louback: [WebRTC portal for the Debian community](https://wiki.debian.org/SummerOfCode2014/StudentApplications/JulianaLouback)
+ Kumar Sukhani: [Integrate Debian with Android](https://wiki.debian.org/SummerOfCode2014/StudentApplications/Kumar%20Sukhani)
+ Lucas Kanashiro: [Debian Continuous Integration](https://wiki.debian.org/SummerOfCode2014/StudentApplications/LucasKanashiro)
+ Pavol Rohár: [mhonarc replacement for lists.debian.org](https://wiki.debian.org/SummerOfCode2014/StudentApplications/PavolRohar)
+ Peter Pentchev: [Bootstrappable Debian](https://wiki.debian.org/SummerOfCode2014/StudentApplications/PeterPentchev)
+ Plamen Aleksandrov: [Improve Debian on mipsel](https://wiki.debian.org/SummerOfCode2014/StudentApplications/PlamenAleksandrov)
+ Sergey Davidoff: [Package elementary software](https://wiki.debian.org/SummerOfCode2014/StudentApplications/SergeyDavidoff)
+ Sphinx Jiang: [Improve Debian on mips64el](https://wiki.debian.org/SummerOfCode2014/StudentApplications/SphinxJiang)
+ Xilin Sun: [Improve Debian on mipsel](https://wiki.debian.org/SummerOfCode2014/StudentApplications/XilinSun)

As always, you will be able to follow their progress on the [SoC coordination mailing-list][debian-soc-ml]

Congratulations to all the students and let's make sure we all have an amazing summer!


[debian-soc-ml]: mailto:soc-coordination@lists.alioth.debian.org
