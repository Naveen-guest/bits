Title: Debian welcomes its 2013 crop of GSoC students!
Slug: welcome-gsoc-students-2013
Date: 2013-05-27 21:45
Author: Nicolas Dandrimont
Tags: announce, gsoc
Status: published


We are proud to announce that 16 students have been accepted to work on improving Debian this summer through the [Google Summer of Code](http://www.google-melange.com/gsoc/homepage/google/gsoc2013)! This is great news, following our 15 accepted students in 2012, and 9 accepted students in 2011.

Here is the list of accepted students and projects:

+ Aarsh Shah: [Improvements to Debian Search and the Search Interface](http://wiki.debian.org/SummerOfCode2013/StudentApplications/AarshShah)
+ Aron Xu: [ZFS on Linux integration](http://wiki.debian.org/SummerOfCode2013/StudentApplications/AronXu)
+ Boris Bobrov: [Debian Metrics Portal](http://wiki.debian.org/SummerOfCode2013/StudentApplications/BorisBobrov)
+ Catalin Usurelu: [Enabling free multimedia real-time communications (RTC) with Debian](http://wiki.debian.org/SummerOfCode2013/StudentApplications/CatalinUsurelu)
+ Eleanor Chen: [MIPS N32/N64 ABI Port](http://wiki.debian.org/SummerOfCode2013/StudentApplications/EleanorChen)
+ Emmanouil Kiagias: [Redesign metapackage creation for Debian Blends](http://wiki.debian.org/SummerOfCode2013/StudentApplications/EmmanouilKiagias)
+ Eugenio Cano-Manuel: [Leiningen & Clojure packaging](http://wiki.debian.org/SummerOfCode2013/StudentApplications/EugenioCanoManuel)
+ Fabian Grünbichler: [Add CROTP support to oath-toolkit and dynalogin](http://wiki.debian.org/SummerOfCode2013/StudentApplications/FabianG)
+ Gustavo Alkmim: [Bootstrappable Debian](http://wiki.debian.org/SummerOfCode2013/StudentApplications/GustavoAlkmim)
+ Justus Winter: [Debian GNU/Hurd Debianish initialization](http://wiki.debian.org/SummerOfCode2013/StudentApplications/JustusWinter)
+ Lei Wang: [OpenRC init system in Debian](http://wiki.debian.org/SummerOfCode2013/StudentApplications/LeiWang)
+ Léo Cavaillé: [scan-build on the Debian archive](http://wiki.debian.org/SummerOfCode2013/StudentApplications/LeoCavaille)
+ Marko Lalic: [PTS rewrite in Django](http://wiki.debian.org/SummerOfCode2013/StudentApplications/MarkoLalic)
+ Pawel Sarbinowski: [Debian Android Application](http://wiki.debian.org/SummerOfCode2013/StudentApplications/PawelSarbinowski)
+ Shuxiong Ye: [OpenJDK and Debian](http://wiki.debian.org/SummerOfCode2013/StudentApplications/ShuxiongYe)
+ Simon Chopin: [Implementation of message passing in the Debian infrastructure](http://wiki.debian.org/SummerOfCode2013/StudentApplications/SimonChopin)

If you're interested in one of the projects, please follow the links and talk directly to the students or the mentors, or come hang out with us on [IRC](irc://irc.debian.org/debian-gsoc).

Welcome everyone, and let's make sure we all have an amazing summer!
