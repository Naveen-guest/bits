Title: Call for Proposals for DebConf17 Open Day
Slug: dc17-openday-cfp
Date: 2017-04-18 09:00
Author: DebConf team
Tags: debconf17, debconf, openday
Status: published

The DebConf team would like to call for proposals for the DebConf17 Open Day, 
a whole day dedicated to sessions about Debian and Free Software, 
and aimed at the general public. Open Day will preceed DebConf17 
and will be held in Montreal, Canada, on August 5th 2017.

DebConf Open Day will be a great opportunity for users, developers 
and people simply curious about our work to meet and learn about 
the Debian Project, Free Software in general and related topics.

# Submit your proposal

We welcome submissions of workshops, presentations or any other activity 
which involves Debian and Free Software. 
Activities in both English and French are accepted.

Here are some ideas about content we'd love to offer during Open Day. 
This list is not exhaustive, feel free to propose other ideas!

* An introduction to various aspects of the Debian Project
* Talks about Debian and Free Software in art, education and/or research
* A primer on contributing to Free Software projects
* Free software & Privacy/Surveillance
* An introduction to programming and/or hardware tinkering
* A workshop about your favorite piece of Free Software
* A presentation about your favorite Free Software-related project (user group, advocacy group, etc.)

To submit your proposal, please fill the form at 
[https://debconf17.debconf.org/talks/new/](https://debconf17.debconf.org/talks/new/)

# Volunteer

We need volunteers to help ensure Open Day is a success! 
We are specifically looking for people familiar 
with the Debian installer to attend the Debian installfest, 
as resources for people seeking help to install Debian on their devices. 
If you're interested, please add your name to our wiki: 
[https://wiki.debconf.org/wiki/DebConf17/OpenDay#Installfest](https://wiki.debconf.org/wiki/DebConf17/OpenDay#Installfest)

# Attend

Participation to Open Day is free and no registration is required.

The schedule for Open Day will be announced in June 2017.

![DebConf17 logo](|filename|/images/800px-Dc17logo.png)

