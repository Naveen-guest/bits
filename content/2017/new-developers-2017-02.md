Title: New Debian Developers and Maintainers (January and February 2017)
Slug: new-developers-2017-02
Date: 2017-03-08 00:30
Author: Jean-Pierre Giraud
Tags: project
Status: published

The following contributors got their Debian Developer accounts in the last two months:

  * Ulrike Uhlig (ulrike)
  * Hanno Wagner (wagner)
  * Jose M Calhariz (calharis)
  * Bastien Roucariès (rouca)

The following contributors were added as Debian Maintainers in the last two months:

  * Dara Adib
  * Félix Sipma
  * Kunal Mehta
  * Valentin Vidic
  * Adrian Alves
  * William Blough
  * Jan Luca Naumann
  * Mohanasundaram Devarajulu
  * Paulo Henrique de Lima Santana
  * Vincent Prat

Congratulations!

