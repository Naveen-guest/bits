Title: Unknown parallel universe uses Debian
Slug: unknown-parallel-universe-uses-debian
Date: 2017-04-01 15:30
Author: Debian Publicity Team
Tags: debian, announce
Status: published

**This post was an April Fools' Day joke.**

The space agencies running the [International Space Station 
(ISS)](https://en.wikipedia.org/wiki/International_Space_Station_program) 
reported that a laptop accidentally threw to space as waste in 2013 from 
the International State Station may have connected with a parallel Universe. [
This laptop was running Debian 6](https://phys.org/news/2013-05-international-space-station-laptop-
migration.html) and the ISS engineers managed to  track its travel 
through the outer space. In early January, the laptop signal was lost 
but recovered back two weeks later in the same place. ISS engineers 
suspect that the laptop may had met and crossed a 
[wormhole](https://en.wikipedia.org/wiki/Wormhole) arriving a parallel 
Universe from where "somebody" sent it back later.

Eventually the laptop was recovered and in an first analysis the ISS 
engineers found that the laptop have a dual boot: a partition running 
the Debian installation made by them and a second partition running 
what seems to be a Debian fork or 
[derivative](https://wiki.debian.org/Derivatives/) totally unknown 
until now.

The engineers have been in contact with the Debian Project in the last 
weeks and a Debian group formed with delegates from different Debian 
teams have begun to study this new Debian derivative system. From the 
early results of this research, we can proudly say that somebody (or a 
group of beings) in a parallel universe understand Earth computers, 
and Debian, enough to:

* Clone the existing Debian system in a new partition and provide a 
dual boot using Grub.
* Change the desktop wallpaper from the previous 
[Spacefun theme](https://wiki.debian.org/DebianArt/Themes/SpaceFun) 
to one in rainbow colors.
* Fork all the packages whose 
[source code](https://sources.debian.net/) was present in the initial 
Debian system, patch multiple bugs in those packages and some patches 
more for some tricky security problems.
* Add ten new language locales that do not correspond to any language 
spoken in Earth, with full translation for four of them.
* A copy of 
[the Debian website repository](https://anonscm.debian.org/viewvc/webwml/), 
migrated to the git version control system and perfectly running, 
has been found in the _/home/earth0/Documents_ folder. This new repo 
includes code to show the Debian [micronews](https://micronews.debian.org/) 
in the home page and many other improvements, keeping the style of not needing 
JavaScript and providing a nice control of up-to-date/outdated 
translations, similar to the one existing in Debian.

The work towards knowing better this new Universe and find a way to 
communicate with them has just began; all the Debian users and 
contributors are invited to join the effort to study the operating 
system found. We want to prepare our Community and our Universe to 
live and work peacefully and respectfully with the parallel Universe 
communities, in the true spirit of Free Software.

In the following weeks a General Resolution will be proposed for 
updating our motto to "the multiversal operating system".
