Title: Debsources now in sources.debian.org
Date: 2017-12-13 18:40
Tags: mirror, announce, sources
Slug: debsources-now-in-sources-debian-org
Author:  Laura Arjona Reina
Status: published

[**Debsources**](https://salsa.debian.org/qa/debsources) is a
 web application for publishing, browsing and searching an unpacked Debian source
mirror on the Web. With Debsources, all the source code of every Debian
release is available in [https://sources.debian.org](https://sources.debian.org/),
both via an HTML user interface 
and a [JSON API](https://sources.debian.org/doc/api/).

This service was first offered in 2013 with the _sources.debian.net_ instance, 
which was kindly hosted by [IRILL](http://www.irill.org), and is now becoming
official under **sources.debian.org**, hosted on the Debian infrastructure.

This new instance offers all the features of the old one (an updater that runs four
times a day, various plugins to count lines of code or measure the size of packages, 
and sub-apps to show lists of patches and copyright files), plus integration 
with other Debian services such as [codesearch.debian.net](https://codesearch.debian.net) 
and the [PTS](http://packages.qa.debian.org/).

The Debsources Team has taken the opportunity of this move of Debsources onto
the Debian infrastructure to officially announce the service. 
Read their [message](https://lists.debian.org/debian-devel-announce/2017/12/msg00000.html)
as well as [the Debsources documentation page](https://sources.debian.org/doc/about/) 
for more details.
