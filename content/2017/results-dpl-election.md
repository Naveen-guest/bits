Title: DPL elections 2017, congratulations Chris Lamb!
Date: 2017-04-16 18:40
Tags: dpl
Slug: results-dpl-elections-2017
Author: Laura Arjona Reina
Status: published

The Debian Project Leader elections finished yesterday and the winner is Chris Lamb!

Of a total of 1062 developers, 322 developers voted using the [Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the result is available in the [Debian Project Leader Elections 2017 page](https://www.debian.org/vote/2017/vote_001).

The current Debian Project Leader, Mehdi Dogguy, congratulated Chris Lamb in his 
[Final bits from the (outgoing) DPL message](https://lists.debian.org/debian-project/2017/04/msg00041.html).
Thanks, Mehdi, for the service as DPL during this last twelve months!

The new term for the project leader starts on April 17th and expires on April 16th 2018.


