Title: Sprint del grupo Debian Ruby 2015
Slug: ruby-sprint-2015
Date: 2015-05-12 00:01
Author: Antonio Terceiro
Translator: Daniel Cialdella
Tags: ruby, sprint, paris, irill
Lang: es
Status: published

El equipo de [Ruby de Debian](https://wiki.debian.org/Teams/Ruby) tuvo su primer sprint en 2014. La 
experiencia fue muy positiva, y se decidió hacerlo nuevamente 
en 2015. El pasado abril, una vez más, el grupo se reunió en las oficinas 
de [IRILL](http://www.irill.org/), en París, Francia.


Los participantes trabajaron para mejorar la calidad de los paquetes 
de Ruby en Debian, incluyendo correcciones de fallos críticos de la versión y 
errores de seguridad, mejorando los meta datos y el código del 
empaquetado y gestionando los fallos de las pruebas sobre el 
servicio de [Integración Continua de Debian](https://ci.debian.net).

El sprint también sirvió para preparar la infraestructura del grupo 
para la futura versión Debian 9:

- el ayudante de empaquetado `gem2deb` para a mejorar la generación 
semi-automática de los paquetes fuentes Debian para los paquetes 
existentes compatibles con los estándares de las 
[gemas de Ruby](https://rubygems.org/).


- también se hizo un esfuerzo para preparar el cambio a Ruby 2.2, la 
última versión estable del lenguaje Ruby que fue publicada después de 
congelarse la versión Debian 8. 

![Foto de los participantes del grupo en el Sprint. De izquierda a derecha: Christian Hofstaedtler, Tomasz Nitecki, Sebastien Badia y Antonio Terceiro](|filename|/images/ruby-sprint-2015.jpg)

De izquierda a derecha: Christian Hofstaedtler, Tomasz Nitecki, Sebastien 
Badia y Antonio Terceiro.


Se ha publicado un [informe completo](https://lists.debian.org/debian-ruby/2015/05/msg00024.html) 
con los detalles técnicos 
en las correspondientes listas de correos de Debian.

