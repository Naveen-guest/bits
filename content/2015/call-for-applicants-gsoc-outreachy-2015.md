Title: Debian is now welcoming applicants for Outreachy and GSoC Summer 2015
Date: 2015-03-16 21:45
Tags: announce, gsoc, outreachy
Slug: call-for-applicants-gsoc-outreachy-2015
Author: Ana Guerrero Lopez
Status: published

**We'd like to reshare [a post from Nicolas Dandrimont](http://blog.olasd.eu/2015/03/debian-welcomes-applicants-for-outreachy-and-gsoc-summer-2015/).**

Hi all,

I am delighted to announce that Debian will be participating in the next round
of [Outreachy](https://wiki.debian.org/Outreachy/Round10)
and [GSoC](https://wiki.debian.org/SummerOfCode2015),
and that we are currently welcoming applications!

![Outreachy logo](|filename|/images/outreachy-logo-300x61.png)

[Outreachy](https://www.gnome.org/outreachy/)
helps people from groups underrepresented in free and open source
software get involved. The current round of internships is open to women (cis
and trans), trans men, genderqueer people, and all participants of the [Ascend
Project](http://ascendproject.org/) regardless of gender.


![GSoC 2015 logo](|filename|/images/gsoc2015-300x270-300x270.jpg)

[Google Summer of Code](https://developers.google.com/open-source/soc/)
is a global program, sponsored by Google, that offers
post-secondary student developers ages 18 and older stipends to write code
for various open source software projects.

Interns for both programs are granted a $5500 stipend (in three installments)
allowing them to dedicate their summer to working full-time on Debian.

Our amazing team of mentors has [listed their project ideas on the Debian
wiki](https://wiki.debian.org/SummerOfCode2015/Projects),
and we are now welcoming applicants for both programs.

If you want to apply for an internship with Debian this summer, please fill
out the template for either
[Outreachy](https://wiki.debian.org/Outreachy/Round10/Applications) or
[GSoC](https://wiki.debian.org/SummerOfCode2015/StudentApplications).
If you’re eligible to both
programs, we’ll encourage you to apply to both (using the same application),
as Debian only has funds for a single Outreachy intern this round.

Don’t wait up! The application period for Outreachy ends March 24th, and the
GSoC application period ends March 27th. We really want applicants to start
contributing to their project before making our selection, so that mentors
can get a feel of how working with their intern will be like for three months.
The small task is a requirement for Outreachy, and we’re strongly encouraging
GSoC applicants to abide by that rule too. To contribute in the best
conditions, you shouldn’t wait for the last minute to apply :-)

I hope we’ll work with a lot of great interns this summer. If you think
you’re up for the challenge, it’s time to apply! If you have any doubts,
or any question, drop us a line on the [soc-coordination mailing
list](mailto:soc-coordination@lists.alioth.debian.org) or
come by on our IRC channel ([#debian-soc](irc://irc.debian.org/#debian-soc)
on irc.debian.org) and we’ll do our best to guide you.

