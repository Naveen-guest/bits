Title: DebConf15 welcomes new sponsors
Slug: new-sponsors-debconf15
Date: 2015-03-18 16:00
Author: Laura Arjona Reina
Tags: debconf15, debconf, sponsors
Status: published


The organization of **DebConf15** (from 15 to 22 August 2015, in Heidelberg,
Germany) is going smoothly, the [call for proposals is open](https://bits.debian.org/2015/03/debconf15-cfp.html)
and today we want to provide some updates about our sponsors.

Twelve more companies have joined [our nine first sponsors](https://bits.debian.org/2014/11/dc15-welcome-its-first-sponsors.html)
in supporting DebConf15. Thank you to all of them!

Our third Gold sponsor is the [**Matanel Foundation**](http://www.matanel.org/), which
encourages social entrepreneurship in all over the world.

[**IBM**](http://www.ibm.com), the technology and consulting corporation,
has also joined the DebConf15 sponsorship at a Gold level.

[**Google**](http://google.com), the search engine and advertising company,
has increased its sponsorship level from Silver to Gold.

[**Mirantis**](http://www.mirantis.com/), [**1&1**](http://www.1und1.de/)
(which is also one of Debian's service partners),
[**MySQL**](http://www.mysql.com/) and
[**Hudson River Trading**](http://www.hudson-trading.com/) have committed
sponsorship at Silver level.

And last but not least, six more sponsors have agreed to support us at Bronze
level: [**Godiug.net**](http://www.godiug.net/),
the [**University of Zurich**](http://www.ifi.uzh.ch/),
[**Deduktiva**](http://www.deduktiva.com/),
[**Docker**](http://www.docker.com/),
[**DG-i**](http://www.dg-i.de/) (which is also one of Debian's service partners),
and [**PricewaterhouseCoopers**](http://www.pwc.de/) (which also provides
consultancy support for DebConf15).

The DebConf15 team is very thankful to all the DebConf sponsors for their support.

## Become a sponsor too!

DebConf15 is still accepting sponsors. Interested companies and organizations
may contact the DebConf team through [sponsors@debconf.org](mailto:sponsors@debconf.org),
and visit the DebConf15 website at [http://debconf15.debconf.org](http://debconf15.debconf.org).

