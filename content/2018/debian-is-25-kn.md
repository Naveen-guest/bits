Title: ಡೆಬಿಯನ್ಗೆ ಈಗ ೨೫ ವಸಂತಗಳ ಸಂಭ್ರಮ.
Slug: debian-is-25 
Date: 2018-08-31 9:40
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, birthday
Translator: Naveen Kumar M
Lang: Kn

![ಶೀರ್ಷಿಕೆ : ಡೆಬಿಯನ್ಗೆ ಈಗ ೨೫ ವಸಂತಗಳ ಸಂಭ್ರಮ.](|filename|/images/debian25years.png)

೨೫ ವರ್ಷದ ಹಿಂದೆ ಲೇಟ್ ಮುರಡೋಕ್ ಡೆಬಿಯನ್ ಅನ್ನು ....... ಅಲ್ಲಿ ಪ್ರಕಟಿಸಿದಾಗ ಮುಂದೊಂದು ದಿನ *"ಡೆಬಿಯನ್ ಲಿನಕ್ಸ್ ರಿಲೀಸ್" ನಿಂದ "ಡೆಬಿಯನ್ ಪ್ರಾಜೆಕ್ಟ್" * , ಆಗುತ್ತದೆ ಎಂದು ಯಾರು ಇದನ್ನು ಊಹಿಸಿರಲ್ಲಿಲ್ಲ. 
ಇಂದು ಬಹು ದೊಡ್ಡ ಹಾಗೂ ಪ್ರಭಾವಿಯೂತವಾದ ಫ್ರೀ ಸಾಫ್ಟ್‌ವೇರ್ ಪ್ರಾಜೆಕ್ಟ್ ಇದಾಗಿದೆ. ಇದರ ಬಹು ಮುಖ್ಯ ಪ್ರಾಜೆಕ್ಟ್ ಡೆಬಿಯನ್ ಆಪರೇಟಿಂಗ್ ಸಿಸ್ಟಮ್ ಆಗಿದ್ದು ಉಳಿದೆಲ್ಲ ಪ್ರಾಜೆಕ್ಟ್ಗಳು ನುಮ್ಮ ದೈನಂದಿನ ಕೆಲಸಗಳನ್ನು ಉತ್ತಮಗೊಳಿಸಲು ನೆರವಾಗುತ್ತವೆ. 

ನಿಮ್ಮ ಹತ್ತಿರದಲ್ಲಿನ ವಿಮಾನ ನಿಲ್ದಾಣದಲ್ಲಿನ ಸಣ್ಣ ಸಣ್ಣ ತಾಂತ್ರಿಕ ಕೆಲಸಗಳಿಂದ ಹಿಡಿದು ದಿನ ನಿತ್ಯ ಓಡಾಡುವ ಕಾರಿನ ಆಡಿಯೋ ಪ್ಲೇಯರ್ ವರೆಗೂ ಮತ್ತು  ಮನೆಯೊಳಗೆ ಇರುವ ಐ.ಓ.ಟೀ ಪರಿಕ್ರಮಗಳು ಹಾಗೂ ಇವುಗಳಿಗೆ ಸಂದೇಶ್ ರವಾನಿಸುವ ಕ್ಲೌಡ್ ಸರ್ವರ್ ನಲ್ಲಿ ಇರುವ ವೆಬ್‌ಸೈಟ್ಗಳೆಲ್ಲವಕ್ಕೂ ಇದು ಜೀವ ತುಂಬುತ್ತದೆ.

ಇಂದು, ಡೆಬಿಯನ್ ಯೋಜನೆಯು ಲೆಕ್ಕವಿಲ್ಲದಷ್ಟು ಸ್ವಯಂ-ಸಂಘಟಿತವಾದ ಸ್ವಯಂಸೇವಕರು ಒಳಗೊಂಡಿರುವ ದೊಡ್ಡ ಮತ್ತು ಅಭಿವೃದ್ಧಿ ಹೊಂದಿದ ಸಂಘಟನೆಯಾಗಿದೆ.ಇದು ಹೊರಗಿನಿಂದ ಅವ್ಯವಸ್ತೆಯಂತೆ ತೋರುತ್ತದೆಯಾದರೂ, ಯೋಜನೆಯು
	  ಅದರ ಎರಡು ಪ್ರಮುಖ ಸಾಂಸ್ಥಿಕ ದಾಖಲೆಗಳು: ಸಮಾಜವನ್ನು ಸುಧಾರಿಸುವ ಒಂದು ದೃಷ್ಟಿ, ಒದಗಿಸುತ್ತದೆ ಡೆಬಿಯನ್ ಸೊಸೈಟಿ ಕಾಂಟ್ರಾಕ್ಟ್,    ಮತ್ತು ಯಾವ ಸಾಫ್ಟ್ವೇರ್ ಅನ್ನು ಉಪಯೋಗಿಸಬಹುದೆಂದು ಸೂಚನೆಯನ್ನು ಒದಗಿಸುವ ಡೆಬಿಯನ್ ಫ್ರೀ ಸಾಫ್ಟ್ವೇರ್ ಮಾರ್ಗಸೂಚಿಗಳು.
ಪ್ರಾಜೆಕ್ಟ್ ವಿನ್ಯಾಸವನ್ನು ಮತ್ತು ಪ್ರಾಜೆಕ್ಟ್ ನ ಸಂಹಿತೆಗೆ ಅನುಗುಣವಾಗಿ ಪ್ರಾಜೆಕ್ಟ್ ನ ಸಂವಿಧಾನದ ಮೂಲಕ ಇವುಗಳನ್ನು ಪೂರೈಸಲಾಗುತ್ತದೆ , ಹಾಗೂ ಪ್ರೊಜೆಕ್ಟೊಳಗಿನ ಪರಸ್ಪರ ಕ್ರಿಯೆಗಳಿಗೆ ಧ್ವನಿಯನ್ನು ಹೊಂದಿಸುತ್ತದೆ.

ಕಳೆದ ೨೫ ವರ್ಷಗಳಿಂದ ಪ್ರತಿ ದಿನ ಜನರು ದೋಷ ವರದಿಗಳು ಮತ್ತು ಪ್ಯಾಚ್ಗಳನ್ನು ಕಳುಹಿಸಿದ್ದಾರೆ, ಪ್ಯಾಕೇಜುಗಳು ನಿರ್ಮಿಸುವುದು, ನವೀಕೃತ ಅನುವಾದಮಾಡುವುದು, ಕಲಾಕೃತಿ ರಚಿಸುತ್ತಾ, ಡೆಬಿಯನ್ ಬಗ್ಗೆ ಸಂಘಟಿತ ಭೇಟಿಯನ್ನು ಆಯೋಜಿಸಿದ್ದಾರೆ, ವೆಬ್ಸೈಟ್ ನವೀಕರಿಸಿದ್ದಾರೆ , ಡೆಬಿಯನ್ ಅನ್ನು ಹೇಗೆ ಬಳಸಬೇಕೆಂದು ಇತರರಿಗೆ ಕಳಿಸಿದ್ದಾರೆ, ಮತ್ತು ನೂರಾರು ಉತ್ಪನ್ನಗಳನ್ನು ಸೃಷ್ಟಿಸಿದ್ದಾರೆ.

** ೨೫ ವಸಂತಗಳನ್ನು ಪೂರೈಸಿರುವ ಡೆಬಿಯನ್  ಹೀಗೆ ಇನ್ನೂ ಹೆಚ್ಚು ವರ್ಷ ಬೆಳೆದು ಹೆಸರುಗಳಿಸಲೆಂದು ಆಶಿಸೋಣ. **
	
[ಡೆಬಿಯನ್ ಸೋಶಿಯಲ್ ಕಾಂಟ್ರ್ಯಾಕ್ಟ್] : https://www.debian.org/social_contract 

[ಡೆಬಿಯನ್ ಫ್ರೀ ಸಾಫ್ಟವೇರ್ ಗೈಡ್] : https://www.debian.org/social_contract#guidelines

[ಡೆಬಿಯನ್ ಸಂವಿಧಾನ] :  https://www.debian.org/devel/constitution    

[ನೀತಿ ಸಂಹಿತೆ] : https://www.debian.org/code_of_conduct

