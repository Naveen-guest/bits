Title: DebConf18 邀你參加在新竹交通大學電子與資訊研究中心舉辦的 Debian 開放日 (Open Day)
Slug: debconf18-open-day
Date: 2018-07-27 12:00
Author: Laura Arjona Reina, Héctor Orón Martínez
Tags: debconf, debconf18, debian
Lang: zh-TW
Translator: Roger Shimizu
Status: published

![DebConf18 logo](|filename|/images/DebConf18_Horizontal_Logo_600_240.png)

[DebConf](https://debconf18.debconf.org) 將於7月29日至8月5日在新竹的[交大電子與資訊研究中心 (NCTU MIRC)](https://wiki.debconf.org/wiki/DebConf18/Venue) 舉行。DebConf 是 Debian 貢獻者與有興趣之用戶爲了不斷進 [Debian 作業系統](https://www.debian.org) 的年度盛典。在這活動之前，會有另外一個從 7月21日到27日的 DebCamp 活動。DebConf18 的 開放日 (Open Day) 安排在7月28日。

Debian 系統包括完全自由開放的作業系統，作爲傳承 Unix 和自由軟體哲學的具體實現而廣爲人所知。
遍布全球各地的成千上萬的程式師共同創建並維護着 Debian。這次他們中的 400 多人會參與這次年度會議，更加緊密的面對面的推進工作。

會議由講座和工作坊(workshops)組成，並會提供即時串流與稍後公布的錄影片。

7月28日(周6)的 DebConf18 公開日 (Open Day) 是向更廣泛公衆開放的公開活動。

[公開日的行程表](https://wiki.debconf.org/wiki/DebConf18/OpenDay) 包括有:

* 行政院政務委員唐鳳的問&答
* 林上智(SZ Lin) 分享 “當 Debian 遇上城市智能應用”
* Debian 套件打包/發布工作坊(Workshop)
* 多人講座: 關於來自世界各地的 Debian 工程師的故事
* 其他中英文的場次 關於 Debian 專案和社群的各個面向和相關自由軟體專案，例如 LibreOffice, Clonezilla 和 DRBL, LXDE/LXQt desktops, EzGo 等等

歡迎任何人來參加。參加活動是免費的, [Registration](https://debconf18.debconf.org/about/registration/) 這是對 Debian 有興趣人士與 Debian 社群面對面的絕贊機會。

開放日 (Open Day) 和之後的完整行程在[https://debconf18.debconf.org/schedule](https://debconf18.debconf.org/schedule) 影音串流在 [DebConf18 網站](https://debconf18.debconf.org)

DebConf 致力與一個安全與和對全員友善的環境。更多資訊在 [DebConf Code of Conduct](http://debconf.org/codeofconduct.shtml) 和 [Debian Code of Conduct](https://www.debian.org/code_of_conduct)

Debian 感謝各個[贊助商](https://debconf18.debconf.org/sponsors/) 鼎力支持此次 DebConf18, 特別是下列白金贊助商 [Hewlett Packard Enterprise](http://www.hpe.com/engage/opensource), [經濟部國際貿易局](https://www.trade.gov.tw/English/) 的 [遇見臺台灣計劃](https://www.meettaiwan.com/en_US/index.html), 還有贊助場地的 [國立交通大學](http://www.nctu.edu.tw/) 與 [國家高速網路與計算中心](https://www.nchc.org.tw/).

媒體聯繫, 可聯絡 DebConf 聯絡人: 林上智 (SZ Lin), 手機: 0911-162297
