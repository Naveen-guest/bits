Title: Novos desenvolvedores e mantenedores Debian (novembro e dezembro de 2017)
Slug: new-developers-2017-12
Date: 2018-01-01 20:30
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Status: published

Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian
nos últimos dois meses:

  * Ben Armstrong (synrg)
  * Frédéric Bonnard (frediz)
  * Jerome Charaoui (lavamind)
  * Michael Jeanson (mjeanson)
  * Jim Meyering (meyering)
  * Christopher Knadle (krait)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian
nos últimos dois meses:

  * Chris West
  * Mark Lee Garrett
  * Pierre-Elliott Bécue
  * Sebastian Humenda
  * Stefan Schörghofer
  * Stephen Gelman
  * Georg Faerber
  * Nico Schlömer


Parabéns a todos!
