#!/usr/bin/env python
# -*- coding: utf-8 -*- #

# Basic details
AUTHOR = u'The Debian Project'
SITENAME = u'Bits from Debian'
SITESUBTITLE = u'Blog from the Debian Project'
SITEURL = 'https://bits.debian.org'

# Configuration
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = u'en'
DELETE_OUTPUT_DIRECTORY = True
THEME = "theme-bits"
DEFAULT_PAGINATION = 5
DISPLAY_PAGES_ON_MENU = True
SUMMARY_MAX_LENGTH = None
LOCALE='C'

# URL settings
# We might want this for publication
RELATIVE_URLS = True
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_LANG_URL = '{date:%Y}/{date:%m}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = '{date:%Y}/{date:%m}/{slug}-{lang}.html'

# Feeds settings
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/feed.rss'
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TAG_FEED_ATOM = None
TAG_FEED_RSS = None
TRANSLATION_FEED = None
TRANSLATION_FEED_ATOM = 'feeds/atom-%s.xml'
TRANSLATION_FEED_RSS = 'feeds/feed-%s.rss'

# Do not create category pages
CATEGORIES_SAVE_AS = None
CATEGORY_SAVE_AS = ''
CATEFORY_URL = None

MENUITEMS =  (('Home', '/'),)

SOCIAL = (('identi.ca/debian', 'https://identi.ca/debian'),
          ('Debian Project News', 'https://www.debian.org/News/'),
          ('Debian micronews ','https://micronews.debian.org/'),
          ('Get Debian', 'http://get.debian.net'))


PATH = 'content'
STATIC_PATHS = [
    'extras/favicon.ico',
	'images',
    ]
EXTRA_PATH_METADATA = {
    'extras/favicon.ico': {'path': 'favicon.ico'},
    }

# Plugins

PLUGINS = ["add_translator_line", "add_artist_line"]
PLUGIN_PATHS = ["plugins"]
